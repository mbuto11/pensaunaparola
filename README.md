# pensaunaparola

A very prototypal ML app

(italian language follows)

INSTALLAZIONE
=============

1. Pre-requisiti:
- Pacchetto sqlitebrowser installato
- Pacchetto golang installato
- Pacchetto git installato

2. Apri una finestra Terminale come utente normale (non root)
3. Esegui i seguenti comandi (ognuno seguito da Invio):

git clone https://gitlab.com/mbuto11/pensaunaparola.git

cd pensaunaparola

go mod init pensaunaparola

go mod tidy

go build

touch pensa.db

4. Assicurati di aver installato SQLite Browser
3. Esegui il comando:
sqlitebrowser pensa.db

5. Nella finestra che si aprirà:
File -> Importa -> Database da file SQL
seleziona: parola.sql Apri
Rispondi NO al prompt

6. Comparirà:
Import Completato OK

7. Salva le modifiche (in cima alla videata)

8. Esci da sqlitebrowser

PER GIOCARE
===========

1. Nella finestra Terminale esegui il programma
./pensaunaparola

2. Appariranno due messaggi (data e ora saranno diverse):
2023/04/30 11:41:03 Defaulting to port 8089
2023/04/30 11:41:03 Listening on port 8089

3. In un browser (Chrome, Firefox...) vai alla pagina:
http://localhost:8089

4. Buon Gioco!

