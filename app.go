package main

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"net/http"
	"regexp"
	"strconv"
)

type Parola struct {
	Ind  int
	OYes int
	Yes  int
	No   int
	Essa string
}

var globaldb *sql.DB

var zz1 Parola

func init() {
	http.HandleFunc("/", root)
	http.HandleFunc("/help", help)
	http.HandleFunc("/lista", lista)
	http.HandleFunc("/aggio", aggio)
	http.HandleFunc("/nuovo", nuovo)
	http.HandleFunc("/robots.txt", robots)
	http.Handle("/images/", http.StripPrefix("/images/", http.FileServer(http.Dir("images/"))))
	http.Handle("/text/", http.StripPrefix("/text/", http.FileServer(http.Dir("text/"))))

	database, err1 := sql.Open("sqlite3", "pensa.db")
	if err1 != nil {
		fmt.Printf("ERR DB Open %v\n", err1)
		return
	}
	globaldb = database
}

var x int

func root(w http.ResponseWriter, r *http.Request) {

	rr := r.FormValue("sn")

	// sanitizing input
	if !checkalpha(w, rr) {
		return
	}
	if !checkalpha(w, r.FormValue("par")) {
		return
	}

	if rr == "" {
		x = 0
	} else {
		if rr == "s" {
			x, _ = strconv.Atoi(r.FormValue("y"))
		} else {
			x, _ = strconv.Atoi(r.FormValue("n"))
		}
	}

	if x < 0 {
		if rr == "s" {
			fmt.Fprintf(w, formIndov, r.FormValue("par"))
		} else {
			fmt.Fprintf(w, formNon, r.FormValue("par"))
		}
		return
	}

	qry := fmt.Sprintf("select essa, oyes, no from parola where ind = %d", x)
	rows, err := globaldb.Query(qry)
	if err != nil {
		fmt.Fprintf(w, qry)
		return
	}
	len := 0
	for rows.Next() {
		rows.Scan(&zz1.Essa, &zz1.OYes, &zz1.No)
		len++
	}
	rows.Close()

	if len == 0 {
		fallo(w, "gatto", -1, -1)
	} else {
		if zz1.OYes == 0 {
			zz1.OYes = zz1.Yes
		}
		fallo(w, zz1.Essa, zz1.OYes, zz1.No)
	}

}

/***/

func fallo(w http.ResponseWriter, i string, j int, k int) {
	var p string

	if j < 0 {
		p = "parola da te pensata"
	} else {
		p = "mia domanda"
	}
	fmt.Fprintf(w, mioForm0, p, i, j, k, i)

}

func robots(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "User-agent: *\nDisallow: /\n")
}

func aggio(w http.ResponseWriter, r *http.Request) {
	  a := r.FormValue("old")
	  b := r.FormValue("par")

	  // input sanitizing
	  if !checkalpha(w, a) {
	   return
	  }
	  if !checkalpha(w, b) {
	   return
	  }

	  fmt.Fprintf(w, formAggio, b, a, a, b)
}

func nuovo(w http.ResponseWriter, r *http.Request) {
	  if r.FormValue("old") == "" {
	  	fmt.Fprintf(w, "<html><body>")
	  	http.Error(w, "Not Found", 404)
	  	fmt.Fprintf(w, "</body></html>")
	  	return
	  }


	  //servono: old, new, domanda

	  // fmt.Fprintf(w, "old=%s new=%s dom=%s\n",r.FormValue("old"),r.FormValue("new"),r.FormValue("dom"))

	  // 0. calcola nuovonumero1 e nuovonumero2

qry := "select max(ind) m from parola"
	rows, err := globaldb.Query(qry)
	if err != nil {
		return
	}
	mxind := 0
	for rows.Next() {
		rows.Scan(&mxind)
	}
	rows.Close()
	  n1 := mxind + 1
	  n2 := n1 + 1

	  // 1. trova record con Essa = old
	  old := r.FormValue("old")
	  if !checkalpha(w, r.FormValue("old")) {
	   return
	  }
	  if !checkalpha(w, r.FormValue("dom")) {
	   return
	  }

qry2 := fmt.Sprintf("select essa, ind, no, oyes, yes from parola where essa = '%s'", old)
	rows2, err2 := globaldb.Query(qry2)
	if err2 != nil {
		return
	}
	for rows2.Next() {
		rows2.Scan(&zz1.Essa, &zz1.Ind, &zz1.No, &zz1.OYes, &zz1.Yes)
	}
	rows2.Close()

	  // 2. aggiorna con Essa = domanda, OYes = nuovonumero1, No = nuovonumero2
	  zu := Parola{zz1.Ind,n1,n1,n2,r.FormValue("dom")}
qry3 := fmt.Sprintf("update parola set essa = '%s', ind = %d, no = %d, oyes = %d, yes = %d where essa = '%s'", zu.Essa, zu.Ind, zu.No, zu.OYes, zu.Yes, old)
_, err3 := globaldb.Exec(qry3)
	if err3 != nil {
		return
	}

	  // 3. crea record con Num = nuovonumero1, Essa = new, OYes = No = -1
	  new := r.FormValue("new")
	  uu := Parola{n1,-1,-1,-1,new}
qry4 := fmt.Sprintf("insert into parola (essa, ind, no, oyes, yes) values ('%s', %d, %d, %d, %d)", uu.Essa, uu.Ind, uu.No, uu.OYes, uu.Yes)
_, err4 := globaldb.Exec(qry4)
	if err4 != nil {
		return
	}

	  // 4. crea record con Num = nuovonumero2, Essa = old, OYes = No = -1
	  u2 := Parola{n2,-1,-1,-1,old}
qry5 := fmt.Sprintf("insert into parola (essa, ind, no, oyes, yes) values ('%s', %d, %d, %d, %d)", u2.Essa, u2.Ind, u2.No, u2.OYes, u2.Yes)
_, err5 := globaldb.Exec(qry5)
	if err5 != nil {
		return
	}
	  fmt.Fprintf(w, fineForm, new)

	  // aggiorna hitparade
//	  hit1(w, r)
}

func checkalpha(w http.ResponseWriter, x string) bool {
	if m, _ := regexp.MatchString("^[a-zA-Zèéòùìà' ]*$", x); !m {
		fmt.Fprintf(w, "<html><head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">")
		fmt.Fprintf(w, "<style>.red {background-color: red}</style></head>")
		fmt.Fprintf(w, "<body><span class=red>ERRORE</span>:<b>Sono ammessi solo caratteri alfabetici e spazi.</b><p>")
		fmt.Fprintf(w, "<a href='javascript:history.back()'>correggi l'errore</a><p>")
		fmt.Fprintf(w, "<a href=/>Ricomincia da capo</a>")
		fmt.Fprintf(w, "</body></html>")
		return false
	}
	return true
}

func help(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, formHelp)
}

func lista(w http.ResponseWriter, r *http.Request) {
	//var di string
	//var cl string

	np := 0
	fmt.Fprintf(w, formLista1)
	qry := "select essa from parola where oyes < 0 order by oyes, essa"
	rows, err := globaldb.Query(qry)
	if err != nil {
		return
	}
	for rows.Next() {
		rows.Scan(&zz1.Essa)
		fmt.Fprintf(w, formLista2, zz1.Essa)
		np++
	}
	rows.Close()

	fmt.Fprintf(w, formLista3, np)

}

const mioForm0 = `
<html>
  <head>
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>pensaunaparola</title>
   <link rel="shortcut icon" href=/images/app.ico>
  <link rel='stylesheet' href='/text/w3.css'>
   <style>
.box { border: 1px solid; width: 300px; padding: 5px; border-radius: 15px; }
.evi { background-color: yellow; }
   </style>
  </head>
  <body><div class='w3-container w3-large'>
<h1>Pensa una parola</h1>
<img src=/images/par.jpg>
<p>La %s &egrave;:<p>
<blockquote>
%s?
<p><form method=post>
<button class='w3-button w3-border w3-border-blue' type=submit name='sn' value='s'>S&igrave;</button>
&nbsp;&nbsp;&nbsp;<button class='w3-button w3-border w3-border-blue' type=submit name='sn' value='n'>No</button>
<input type=hidden name=y value=%d>
<input type=hidden name=n value=%d>
<input type=hidden name=par value="%s">
</form>
</blockquote>
<div class=box>
<a class='w3-button w3-border w3-border-blue' href=/help>Serve aiuto?</a><p>
<a class='w3-button w3-border w3-border-blue' href=/lista>Lista delle parole che conosco</a><p>
</div>
</div></body></html>`

const formIndov = `
<html><head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>pensaunaparola</title>
<link rel="shortcut icon" href=/images/app.ico>
  <link rel='stylesheet' href='/text/w3.css'>
<style>
.box { border: 1px solid; width: 300px; padding: 5px; border-radius: 15px; }
</style>
</head><body><div class='w3-container w3-large'>
<h1>Pensa una parola</h1>
<img src=/images/par.jpg>
<h2>Ho indovinato!</h2>
La parola pensata era: <b>%s!</b>
<p>
Grazie per aver giocato.<br>
<a class='w3-button w3-border w3-border-blue' href=/>Gioca ancora</a>
<div class=box>
<a class='w3-button w3-border w3-border-blue' href=/help>Serve aiuto?</a><p>
<a class='w3-button w3-border w3-border-blue' href=/lista>Lista delle parole che conosco</a><p>
</div>
</div></body></html>`

const formNon = `
<html><head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>pensaunaparola</title>
<link rel="shortcut icon" href=/images/app.ico>
  <link rel='stylesheet' href='/text/w3.css'>
</head><body><div class='w3-container w3-large'>
<h1>Pensa una parola</h1>
<img src=/images/par.jpg>
<h2>Non ho indovinato...</h2>
Aiutami ad imparare:<br>
Qual era la parola pensata?
<form method=post action=/aggio>
<input type=text name=par autofocus>
<input type=hidden name=old value="%s">
<button class='w3-button w3-border w3-border-blue' type=submit>Invio</button>
</form>
</div></body></html>`

const formAggio = `
<html><head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>pensaunaparola</title>
<link rel="shortcut icon" href=/images/app.ico>
  <link rel='stylesheet' href='/text/w3.css'>
</head><body><div class='w3-container w3-large'>
<h1>Pensa una parola</h1>
<img src=/images/par.jpg>
<p>Ora dimmi una <b>domanda</b> per distinguere
<br><b>%s</b> (risposta <i>S&igrave;</i>) da <b>%s</b> (risposta <i>No</i>):<br>
<form method=post action=/nuovo>
<input type=text name=dom autofocus>
<input type=hidden name=old value="%s">
<input type=hidden name=new value="%s">
<button class='w3-button w3-border w3-border-blue' type=submit>Invio</button>
</form>
</div></body></html>`

const fineForm = `
<html><head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>pensaunaparola</title>
<link rel="shortcut icon" href=/images/app.ico>
  <link rel='stylesheet' href='/text/w3.css'>
<style>
.box { border: 1px solid; width: 300px; padding: 5px; border-radius: 15px; }
</style>
</head><body><div class='w3-container w3-large'>
<h1>Pensa una parola</h1>
<img src=/images/par.jpg>
<h2>Ho imparato una parola nuova!</h2>
<blockquote><b>%s</b></blockquote>
Grazie, a presto!
<p>
Grazie per aver giocato.<br>
<a class='w3-button w3-border w3-border-blue' href=/>Gioca ancora</a>
<div class=box>
<a class='w3-button w3-border w3-border-blue' href=/help>Serve aiuto?</a><p>
<a class='w3-button w3-border w3-border-blue' href=/lista>Lista delle parole che conosco</a><p>
</div>
</div></body></html>`

const formHelp = `
<html><head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>pensaunaparola</title>
<link rel="shortcut icon" href=/images/app.ico>
  <link rel='stylesheet' href='/text/w3.css'>
</head><body><div class='w3-container w3-large'>
<h1>Pensa una parola</h1>
<img src=/images/par.jpg>
<p><b>Pensaunaparola</b> &egrave; un passatempo.<p>
Tu pensi una parola e io tento di indovinarla.<br>
Se non la indovino, ti chiedo di insegnarmi<br>
a distinguerla da quelle che conosco gi&agrave;.
<p><a class='w3-button w3-border w3-border-blue' href=/>Inizia a giocare!</a>
<p><a class='w3-button w3-border w3-border-blue' href=/lista>Quali parole conosco?</a>
<h2>Privacy e Cookie Policy</h2>
Questo sito non utilizza nessun "cookie"<br>
e non conserva nessun dato dell'utente.
</div></body></html>`

const formLista1 = `
<html><head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>pensaunaparola</title>
<link rel="shortcut icon" href=/images/app.ico>
  <link rel='stylesheet' href='/text/w3.css'>
<style>
th, td {padding: 5px; }
.evi { background-color: yellow; }
</style>
</head><body><div class='w3-container w3-large'>
<h1>Pensa una parola</h1>
<img src=/images/par.jpg>
<p><a class='w3-button w3-border w3-border-blue' href=/>Inizia a giocare!</a>
<h2>Parole conosciute</h2>
<p><ul class='w3-ul' style='width:40%%'>
`
const formLista2 = `
<li class='w3-border-bottom w3-border-blue'>%s</li>
`
const formLista3 = `
</ul>
<p>Tot. %d parole conosciute.
<p><a class='w3-button w3-border w3-border-blue' href=/>Inizia a giocare!</a>
</div></body></html>`


const formCiao = `
<html><head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>pensaunaparola</title>
<link rel="shortcut icon" href=/images/app.ico>
  <link rel='stylesheet' href='/text/w3.css'>
</head><body><div class='w3-container w3-large'>
<img src=/images/par.jpg>
<h3>Grazie per aver giocato a Pensaunaparola.</h3>
<a class='w3-button w3-border w3-border-blue' href=/>Torna presto</a> a giocare con noi.
</div></body></html>`
