BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "parola" (
	"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"essa"	TEXT,
	"ind"	INTEGER,
	"no"	INTEGER,
	"oyes"	INTEGER,
	"yes"	INTEGER
);
INSERT INTO "parola" ("id","essa","ind","no","oyes","yes") VALUES (1,'abbaia',0,2,1,1),
 (2,'vive sui ghiacci',1,4,3,3),
 (3,'serve per scrivere',2,6,5,5),
 (4,'foca',3,-1,-1,-1),
 (5,'cane',4,-1,-1,-1),
 (6,'si impugna',5,8,7,7),
 (7,'illumina e riscalda',6,10,9,9),
 (8,'penna',7,-1,-1,-1),
 (9,'carta',8,-1,-1,-1),
 (10,'sembra che non ci sia',9,18,17,17),
 (11,'si vede soprattutto di notte ma non tutte le notti',10,12,11,11),
 (12,'luna',11,-1,-1,-1),
 (13,'studia tanto',12,14,13,13),
 (14,'studente',13,-1,-1,-1),
 (15,'studia poco',14,16,15,15),
 (16,'studente somaro',15,-1,-1,-1),
 (17,'trasmette immagini e suoni',16,20,19,19),
 (18,'poesia',17,-1,-1,-1),
 (19,'sole',18,-1,-1,-1),
 (20,'televisione',19,-1,-1,-1),
 (21,'trasmette parole e suoni',20,22,21,21),
 (22,'radio',21,-1,-1,-1),
 (23,'er Pupone',22,24,23,23),
 (24,'Totti',23,-1,-1,-1),
 (25,'si mangia',24,26,25,25),
 (26,'cibo',25,-1,-1,-1),
 (27,'è la festa coi coriandoli',26,28,27,27),
 (28,'carnevale',27,-1,-1,-1),
 (29,'mangia le ghiande',28,30,29,29),
 (30,'scoiattolo',29,-1,-1,-1),
 (31,'quando cade bagna',30,32,31,31),
 (32,'pioggia',31,-1,-1,-1),
 (33,'lavora in una redazione',32,34,33,33),
 (34,'giornalista',33,-1,-1,-1),
 (35,'soffia ma non miagola',34,36,35,35),
 (36,'vento',35,-1,-1,-1),
 (37,'gatto',36,-1,-1,-1);
CREATE UNIQUE INDEX IF NOT EXISTS "ind2" ON "parola" (
	"ind"
);
CREATE INDEX IF NOT EXISTS "ind1" ON "parola" (
	"oyes",
	"essa"
);
COMMIT;
